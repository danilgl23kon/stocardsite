openapi: 3.0.3
info:
  title: Swagger Stocard - OpenAPI 3.0
  description: |-
    Сайт для хранения всех скидочных карт в одном месте
  termsOfService: http://swagger.io/terms/
  contact:
    email: test@gmail.com
  version: 1.0.11
tags:
  - name: user
    description: Operations about user
  - name: shops
    description: Operations about shops
paths:
  /user:
    get:
      tags:
        - user
      summary: Get info user by token
      description: ''
      parameters:
        - name: token
          in: header
          description: ''
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '404':
          description: User not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserNotFound'
    post:
        tags:
          - user
        summary: Create user by login and password or sms
        requestBody:
          description: Created user object
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserAuthPassword'
        responses:
          '200':
            description: successful operation
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/User'
          '400':
            description: UserAlreadyExist
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserAlreadyExist'
  /user/sendSms:
    post:
      tags:
          - user
      summary: Create user by phone
      requestBody:
          description: send sms on phone
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSendSmsBody'
      responses:
          default:
            description: successful operation
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserSendSmsResponse'

  /user/confirm:
    post:
      tags:
          - user
      summary: Confirm
      requestBody:
          description: confirm sms
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserAuthSms'
      responses:
          '200':
            description: successful operation
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/UserSendSmsResponse'
          '400':
            description: Invalid code
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/InvalidCode'

  /user/cards:
    get:
      tags:
        - user
      summary: Get own user cards
      parameters:
        - name: token
          in: header
          description: ''
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Shops'
  /user/card-add:
    post:
      tags:
        - user
      summary: Add card to collections
      parameters:
        - name: token
          in: header
          description: ''
          required: true
          schema:
            type: string
      requestBody:
          description: confirm sms
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AddCardToCollections'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Shops'

  /user/delete-card:
    post:
      tags:
        - user
      summary: delete card from collections
      parameters:
        - name: token
          in: header
          description: ''
          required: true
          schema:
            type: string
      requestBody:
          description: delete card from collections
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CardId'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Shops'

  /user/logout:
    post:
      tags:
        - user
      summary: logout from accaunt
      parameters:
        - name: token
          in: header
          description: ''
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnswerResponse'

  /shops/get/all:
    get:
      tags:
        - shops
      summary: Get all stores
      description: ''
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Shops'

  /shops/get:
    get:
      tags:
        - shops
      summary: Get shop by name
      description: ''
      parameters:
        - name: 'name'
          in: query
          description: name of shop
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Shops'
components:
  schemas:
    User:
      type: object
      properties:
        username:
          type: string
          example: theUser
        firstName:
          type: string
          example: John
        lastName:
          type: string
          example: James
        email:
          type: string
          example: john@email.com
        password:
          type: string
          example: '12345'
        phone:
          type: string
          example: '12345'
        userStatus:
          type: integer
          description: User Status
          format: bool
          example: 1
        token:
          type: string
          example: 'IM!ipVrOAKwQKmGljWnihFV/EWhdrovLaWI4fm!hcUSH-'
        responseStatus:
          type: string
          example: 'Succes'
    UserAuthSms:
      type: object
      properties:
        authType:
          type: string
          description: sms or password
          example: 'sms'
        token:
          type: string
          description: token or none
          example: 'IM!ipVrOAKwQKmGljWnihFV/EWhdrovLaWI4fm!hcUSH-'
        code:
          type: string
          description: code or none
          example: '0000'
    UserAuthPassword:
      type: object
      properties:
        username:
          type: string
          example: theUser
        firstName:
          type: string
          example: John
        lastName:
          type: string
          example: James
        email:
          type: string
          example: john@email.com
        password:
          type: string
          example: '12345'
        phone:
          type: string
          example: '12345'
        authType:
          type: string
          description: sms or password
          example: 'password'
        token:
          type: object
          description: token or none
          example: null
        code:
          type: object
          description: code or none
          example: null
    UserNotFound:
      type: object
      properties:
        responseStatus:
          type: string
          example: 'User not found'
    UserSendSmsBody:
      type: object
      properties:
        phone:
          type: string
          example: 79501212139
    UserSendSmsResponse:
      type: object
      properties:
        token:
          type: string
          example: 'IM!ipVrOAKwQKmGljWnihFV/EWhdrovLaWI4fm!hcUSH-'
        is_exist_user:
          type: boolean
          example: false
        responseStatus:
          type: string
          example: 'Succes'
    AddCardToCollections:
      type: object
      properties:
        name:
          type: string
          example: "Пятёрочка"
        value:
          type: string
          example: "22323223323232"
    InvalidCode:
      type: object
      properties:
          result:
            type: boolean
            example: False
          message:
            type: string
            example: "Неправильный код"
    AnswerResponse:
      type: object
      properties:
        result:
          type: boolean
          example: True
        message:
          type: string
          example: "Успешно"
    CardId:
      type: object
      properties:
        cardId:
          type: string
          example: "1234"
    UserAlreadyExist:
      type: object
      properties:
          message:
            type: string
            example: "Пользователь уже существует"
    shop:
      type: object
      properties:
        name:
          type: string
          example: "Пятёрочка"
        value:
          type: string
          example: null
        type:
          type: string
          example: "EAN-13"
        backgraund:
          type: string
          description: base64->bytes->image/png
          example: "iVBORw0KGgoAAAANSUhEUgAAASwAAACLBAMAAAApPbmkAAAAD1BMVEW3IkS8Y1ijyzPfnKv=="
        icon:
          type: string
          description: base64->bytes->image/png
          example: "iVBORw0KGgoAAAANSUhEUgAAASwAAACLBAMAAAApPbmkAAAAD1BMVEW3IkS8Y1ijyzPfnKv=="
    Shops:
      type: object
      properties:
        shops:
          type: array
          items:
            $ref: '#/components/schemas/shop'
