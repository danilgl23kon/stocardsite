from django.urls import path
from user.views import *


app_name = 'UserConfig'

urlpatterns = [
    path("login/", ShowMainPageView.as_view()),
    path("login/<str:action>/", ShowMainPageView.as_view()),
    path("shops/get/all", get_all_card),
    path('user/', ProfileView.as_view()),
    path('user/<str:action>/', ProfileView.as_view()),
    path('user/sendSms', send_sms),
    path('user/confirm', confirm),
]
