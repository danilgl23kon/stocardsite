from django.db import models
from asgiref.sync import sync_to_async
from django.contrib.auth.models import User


from stocard.settings import (BAD, GOOD)


class UserStocard(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=256, unique=True, blank=False)
    first_name = models.CharField(max_length=256, blank=False)
    last_name = models.CharField(max_length=256)
    email = models.CharField(max_length=256)
    password = models.CharField(max_length=128)
    phone = models.CharField(max_length=128)
    registration_date = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create_sync(cls, username, first_name, last_name, email, password, phone, token):
        try:
            new_user = cls(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password,
                phone=phone,
            )
            new_user.save()
            Token().create_sync(new_user, token)
            return GOOD
        except Exception as e:
            print(f"Error: {e}")
            return BAD

    @classmethod
    def get_all_sync(cls, username, password):
        try:
            return cls.objects.get(username=username, password=password)
        except cls.DoesNotExist:
            return BAD


class Token(models.Model):
    user = models.OneToOneField(UserStocard, on_delete=models.CASCADE, related_name='token')
    access_token = models.CharField(max_length=512, blank=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Token for {self.user.username}"

    @classmethod
    @sync_to_async
    def get_all_async(cls, token):
        try:
            return cls.objects.get(access_token=token).user
        except cls.DoesNotExist:
            return BAD

    @classmethod
    def get_all_sync(cls, token):
        try:
            return cls.objects.get(access_token=token).user
        except cls.DoesNotExist:
            return BAD

    @classmethod
    def delete_token(cls, token):
        try:
            token = cls.objects.get(access_token=token)
            token.delete()
            return GOOD
        except cls.DoesNotExist:
            return BAD

    @classmethod
    def create_sync(cls, user, access_token):
        try:
            new_token = cls(
                user=user,
                access_token=access_token
            )
            new_token.save()
            return GOOD
        except Exception as e:
            print(f"Error: {e}")
            return BAD

    @classmethod
    @sync_to_async
    def update_base_async(cls, username, first_name, last_name, email, password, token):
        try:
            obj = cls.objects.get(access_token=token).user
            obj.username = username
            obj.first_name = first_name
            obj.last_name = last_name
            obj.email = email
            obj.password = password
            obj.save()
            return GOOD
        except cls.DoesNotExist:
            return BAD


class Card(models.Model):
    user = models.ForeignKey(UserStocard, on_delete=models.CASCADE) # Ссылка на пользователя
    title = models.CharField(max_length=128, blank=False)
    shop_id = models.CharField(max_length=128, default="", blank=False)
    card_id = models.CharField(max_length=128, blank=False)
    code_card = models.CharField(max_length=256, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    @classmethod
    @sync_to_async
    def get_all_by_user_async(cls, user):
        try:
            return cls.objects.get(user=user)
        except cls.DoesNotExist:
            return BAD

    @classmethod
    def get_all_by_user_sync(cls, user):
        try:
            return cls.objects.filter(user=user)
        except cls.DoesNotExist:
            return BAD

    @classmethod
    def create_sync(cls, user, title, shop_id, card_id, code_card):
        try:
            new_card = cls(
                user=user,
                title=title,
                shop_id=shop_id,
                card_id=card_id,
                code_card=code_card,
            )
            new_card.save()
            return GOOD
        except Exception as e:
            print(f"Error: {e}")
            return BAD
