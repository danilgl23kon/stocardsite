from django.test import TestCase, RequestFactory, Client


RESPONSE_OK = 200
REDiRECT_OK = 302


class TestViews(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    def test_get_user(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, RESPONSE_OK)

    def test_send_sms(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, RESPONSE_OK)

    def test_confirm(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, RESPONSE_OK)

