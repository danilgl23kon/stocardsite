from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from user.models import (UserStocard, Card, Token)
from stocard.settings import (BAD, GOOD)

from uuid import uuid4

import string
import secrets
import json
import base64
from PIL import Image
from io import BytesIO


PATH_ALL_SHOPS = "user/static/shops.json"
PATH_MAIN_PAGE = "user/login.html"
PATH_CARD = "user/card.html"
PATH_PROFILE = "user/profile.html"


def get_random_string(num):
    res = ''.join(secrets.choice(string.ascii_letters + string.digits) for x in range(num))
    return str(res)


class ShowMainPageView(View):
    @staticmethod
    def is_authenticated(token):
        data = Token().get_all_sync(token)
        if data == BAD:
            return False
        return True

    def get(self, request, *args, **kwargs):
        """path - /user
        Get info user by token"""
        if "token" in request.COOKIES:
            if self.is_authenticated(request.COOKIES["token"]):
                return ProfileView.as_view()(request)
        return render(request, PATH_MAIN_PAGE)

    def post(self, request, action=None):
        if "token" in request.COOKIES:
            if self.is_authenticated(request.COOKIES["token"]):
                return ProfileView.as_view()(request)
        if action == "sign-up":
            return self.sign_up(request)
        if action == "sign-in":
            return self.sign_in(request)

    def sign_up(self, request):
        token = get_random_string(64)
        js = json.loads(request.body)
        ans = UserStocard().create_sync(username=js["username"],
                                        first_name=js["firstName"],
                                        last_name=js["lastName"],
                                        email=js["email"],
                                        password=js["password"],
                                        phone=js["phone"],
                                        token=token)
        if ans == GOOD:
            response = JsonResponse({"result": True, "message": "Success authorization"})
            response.set_cookie('token', token)
            return response
        response = JsonResponse({"result": False, "message": "Неизвестная ошибка"})
        return response

    def sign_in(self, request):
        token = get_random_string(64)
        js = json.loads(request.body)
        if "username" in js and "password" in js:
            user = UserStocard().get_all_sync(js['username'], js['password'])
            if user == BAD:
                return JsonResponse({"result": False, "message": "Неверный логин или пароль"})
            if Token().create_sync(user, token) == GOOD:
                response = JsonResponse({"result": True, "message": "Success authorization"})
                response.set_cookie('token', token)
                return response
        response = JsonResponse({"result": False, "message": "Неизвестная ошибка"})
        return response


class ProfileView(View):
    user_info = None

    def is_authenticated(self, token):
        self.user_info = Token().get_all_sync(token)
        if self.user_info == BAD:
            return False
        return True

    def get(self, request, action=None):
        if "token" not in request.COOKIES:
            return ShowMainPageView.as_view()(request)
        if not self.is_authenticated(request.COOKIES["token"]):
            return ShowMainPageView.as_view()(request)
        if action == "cards":
            return self.get_cards()
        if action == "card":
            return self.get_card(request)
        return render(request, PATH_PROFILE, {"last_name": self.user_info.last_name, "first_name": self.user_info.first_name,
                                              "username": self.user_info.username, "birth": "", "sex": "",
                                              "phone": self.user_info.phone, "email": self.user_info.email})

    def post(self, request, action=None):
        if "token" not in request.COOKIES:
            return ShowMainPageView.as_view()(request)
        if not self.is_authenticated(request.COOKIES["token"]):
            return ShowMainPageView.as_view()(request)
        if action == "card-add":
            return self.add_card(request)
        if action == "delete-card":
            return self.delete_card(request)
        if action == "logout":
            return self.logout(request)
        return render(request, PATH_PROFILE)

    def get_cards(self):
        """path - /user/cards
        Add card to collections/Get own user cards"""
        cards = Card().get_all_by_user_sync(self.user_info)
        with open(PATH_ALL_SHOPS) as json_file:
            shops = json.load(json_file)
        my_shops = []
        for shop in shops:
            for card in cards:
                if shop['resource_id'] == card.shop_id:
                    my_shops.append({
                        'resource_id': card.card_id,
                        'info': shop['info'],
                        'logo': shop['logo']
                    })
        return JsonResponse(my_shops, safe=False)

    def get_card(self, request):
        """path - /user/card
                get card"""
        cards = Card().get_all_by_user_sync(self.user_info)
        code_card = ""
        barcode_format = ""
        with open(PATH_ALL_SHOPS) as json_file:
            shops = json.load(json_file)
        title, resource_id, icon = "", "", ""
        for shop in shops:
            info = json.loads(shop["info"])
            for card in cards:
                if (shop['resource_id'] == card.shop_id) and (card.card_id == request.GET.get("card_id")):
                    code_card = card.code_card
                    title = info['name'].upper()
                    icon = shop["logo"]
                    barcode_format = info['default_barcode_format']
                    break
        image = Image.open(BytesIO(base64.b64decode(icon)))
        # Получение размеров изображения
        width, height = image.size
        if image.mode == 'P':  # режим палитры
            image = image.convert('RGB')
        # Получение цвета крайнего пикселя
        bottom_right_pixel = image.getpixel((width - 1, height - 1))
        # Проверка режима изображения
        if image.mode == 'RGB':
            # Преобразование цвета в формат HEX
            bottom_right_pixel_hex = "#{:02x}{:02x}{:02x}".format(*bottom_right_pixel)
        elif image.mode == 'RGBA':
            # Преобразование цвета в формат HEX, включая альфа-канал
            bottom_right_pixel_hex = "#{:02x}{:02x}{:02x}{:02x}".format(*bottom_right_pixel)
        elif image.mode == 'L':
            # Для градаций серого (L - luminance)
            bottom_right_pixel_hex = "#{:02x}".format(bottom_right_pixel)
        else:
            # Если режим не поддерживается, выводим сообщение об ошибке
            bottom_right_pixel_hex = "Unsupported image mode: " + image.mode

        return render(request, PATH_CARD, {"icon": icon, "background_color": bottom_right_pixel_hex,
                                           "card": code_card, "title": title, "barcode_format": barcode_format})

    def add_card(self, request):
        js = json.loads(request.body)
        card_id = uuid4()
        with open(PATH_ALL_SHOPS) as json_file:
            shops = json.load(json_file)
        title, resource_id, icon = "", "", ""
        for shop in shops:
            info = json.loads(shop["info"])
            if js["shopId"] == shop['resource_id']:
                title = info['name'].upper()
                break
        Card().create_sync(self.user_info, title, js["shopId"], str(card_id), js["code"])
        return JsonResponse({"result": True, "message": "Успешно", "card_id": str(card_id)}, safe=False)

    def delete_card(self, request):
        js = json.loads(request.body)
        cards = Card().get_all_by_user_sync(self.user_info)
        for card in cards:
            if card.card_id == js["cardId"]:
                card.delete()
                return JsonResponse({"result": True, "message": "Успешно", "card_id": js["cardId"]}, safe=False)
        return JsonResponse({"result": False, "message": "Ошибка", "card_id": js["cardId"]}, safe=False)

    def logout(self, request):
        if Token().delete_token(request.COOKIES["token"]) == GOOD:
            return JsonResponse({"result": True, "message": "Успешно"}, safe=False)
        return JsonResponse({"result": False, "message": "Ошибка"}, safe=False)


async def send_sms():
    """path - /user/sendSms
    Send sms on phone"""
    return JsonResponse({'message': 'Can not get card'})


async def confirm():
    """path - /user/confirm
    Confirm sms"""
    return JsonResponse({'message': 'Can not get card'})


async def get_all_card(request):
    """path - /shops/get/all
        Get all cards"""
    with open(PATH_ALL_SHOPS) as json_file:
        shops = json.load(json_file)
    return JsonResponse(shops, safe=False)





